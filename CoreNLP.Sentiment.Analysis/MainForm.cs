﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CoreNLP.Sentiment.Analysis
{
    /// <summary>
    /// Main form of application.
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Click handler of Analyze Button.
        /// </summary>
        private async void AnalyzeButtonOnClick(object sender, EventArgs e)
        {
            try
            {
                _textForAnalysis.Enabled = false;
                _analyzeButton.Enabled = false;
                _analyzeButton.Text = @"Analyzing...";

                var text = _textForAnalysis.Text;
                
                _result.Text = await Task.Factory.StartNew(
                    () =>
                    {
                        var annotationResult = CoreNlpService.Instance.Annotate(text);
                        return string.Join("\n",
                            annotationResult
                                .Sentences.Select(t => $"Sentence {t.Index}: {t.Sentiment}({t.SentimentValue})"));
                    });
                ColorMarkupTools.Markup(_result, _result.Text.Split('\n').Where(t => !string.IsNullOrWhiteSpace(t)).ToArray());
            }
            catch (Exception exception)
            {
                MessageBox.Show(this, exception.ToString(), @"Analysis error!", MessageBoxButtons.OK);
            }
            finally
            {
                _textForAnalysis.Enabled = true;
                _analyzeButton.Enabled = true;
                _analyzeButton.Text = @"Analyze";
            }
        }

        /// <summary>
        /// Text changed handler of Text For Analysis.
        /// </summary>
        private void TextForAnalysisOnTextChanged(object sender, EventArgs e)
        {
            ColorMarkupTools.Markup(_textForAnalysis,
                CoreNlpService.Instance.GetSentences(_textForAnalysis.Text).Select(t => t.text()).ToArray());
        }
    }
}
