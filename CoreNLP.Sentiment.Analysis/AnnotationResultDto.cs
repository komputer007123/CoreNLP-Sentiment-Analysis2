﻿namespace CoreNLP.Sentiment.Analysis
{
    /// <summary>
    /// DTO class for result of annotation.
    /// </summary>
    public sealed class AnnotationResultDto
    {
        /// <summary>
        /// Result of sentences processing.
        /// </summary>
        public SentenceDto[] Sentences { get; set; }
    }
}