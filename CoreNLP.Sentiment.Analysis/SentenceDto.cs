﻿namespace CoreNLP.Sentiment.Analysis
{
    /// <summary>
    /// DTO class for result of sentence processing.
    /// </summary>
    public sealed class SentenceDto
    {
        /// <summary>
        /// Sentence index.
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Sentiment value.
        /// </summary>
        public int SentimentValue { get; set; }

        /// <summary>
        /// String sentiment value. 
        /// </summary>
        public string Sentiment { get; set; }
    }
}
