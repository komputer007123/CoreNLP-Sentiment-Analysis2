﻿namespace CoreNLP.Sentiment.Analysis
{
    public class Singleton<T> where T : class, new()
    {
        public static T Instance => SingletonCreator<T>.CreatorInstance;
        
        protected Singleton()
        {
        }
        
        private static class SingletonCreator<TS> where TS : class, new()
        {
            // ReSharper disable once StaticMemberInGenericType
            private static readonly object Locker = new object();
            
            private static volatile TS _instance;
            
            public static TS CreatorInstance
            {
                get
                {
                    if (_instance == null)
                    {
                        lock (Locker)
                        {
                            if (_instance == null)
                            {
                                _instance = new TS();
                            }
                        }
                    }

                    return _instance;
                }
            }
        }
    }
}
