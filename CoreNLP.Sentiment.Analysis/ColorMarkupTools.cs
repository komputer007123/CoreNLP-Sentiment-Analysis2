﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace CoreNLP.Sentiment.Analysis
{
    /// <summary>
    /// Tools for markuping text by colors.
    /// </summary>
    public static class ColorMarkupTools
    {
        /// <summary>
        /// Set of colors.
        /// </summary>
        private static readonly Color[] Colors =
        {
            Color.Red,
            Color.SaddleBrown,
            Color.BlueViolet,
            Color.ForestGreen,
            Color.SteelBlue,
            Color.DarkOrange,
            Color.Blue,
            Color.Teal,
            Color.YellowGreen,
            Color.OliveDrab,
            Color.Violet,
            Color.DarkKhaki
        };

        /// <summary>
        /// Endless color generator.
        /// </summary>
        private static IEnumerable<Color> GetColorGenerator()
        {
            while (true)
            {
                foreach (var color in Colors)
                {
                    yield return color;
                }
            }
            // ReSharper disable once IteratorNeverReturns
        }

        /// <summary>
        /// Markup RichTextBox by colors.
        /// </summary>
        /// <param name="richTextBox">RichTextBox for markup.</param>
        /// <param name="subStrings">Substrings for markup.</param>
        public static void Markup(RichTextBox richTextBox, IEnumerable<string> subStrings)
        {
            var prevSelectionStart = richTextBox.SelectionStart;
            var prevSelectionLength = richTextBox.SelectionLength;

            var colors = GetColorGenerator();

            richTextBox.SelectAll();
            richTextBox.SelectionBackColor = Color.White;
            richTextBox.SelectionColor = Color.Black;

            var startSearchIndex = 0;
            foreach (var x in subStrings.Zip(colors, (sentence, color) => new { sentence, color }))
            {
                var indexOfSentence = richTextBox.Text.IndexOf(x.sentence, startSearchIndex, StringComparison.Ordinal);
                richTextBox.Select(indexOfSentence, x.sentence.Length);
                richTextBox.SelectionBackColor = x.color;
                richTextBox.SelectionColor = Color.White;

                startSearchIndex = indexOfSentence + x.sentence.Length;
            }

            richTextBox.Select(prevSelectionStart, prevSelectionLength);
        }
    }
}
