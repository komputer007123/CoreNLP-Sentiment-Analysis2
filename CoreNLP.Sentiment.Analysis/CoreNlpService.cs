﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using edu.stanford.nlp.pipeline;
using edu.stanford.nlp.simple;
using java.io;
using Newtonsoft.Json;

namespace CoreNLP.Sentiment.Analysis
{
    /// <summary>
    /// CoreNLP library accessing service.
    /// </summary>
    public class CoreNlpService : Singleton<CoreNlpService>
    {
        /// <summary>
        /// Annotation pipline.
        /// </summary>
        private readonly StanfordCoreNLP _pipeline;

        /// <summary>
        /// Constructor.
        /// </summary>
        public CoreNlpService()
        {
            // Path to the folder with models extracted from `stanford-corenlp-3.6.0-models.jar`
            var jarRoot = @"..\..\..\";

            // Annotation pipeline configuration
            var props = new java.util.Properties();
            props.setProperty("annotators", "tokenize,ssplit,pos,parse,sentiment");
            props.setProperty("ner.useSUTime", "0");

            // We should change current directory, so StanfordCoreNLP could find all the model files automatically
            //var curDir = Environment.CurrentDirectory;
            Directory.SetCurrentDirectory(jarRoot);
            _pipeline = new StanfordCoreNLP(props);
            //Directory.SetCurrentDirectory(curDir);
        }

        /// <summary>
        /// Process text.
        /// </summary>
        /// <param name="text">Input text.</param>
        /// <returns>Result of processing of text.</returns>
        public AnnotationResultDto Annotate(string text)
        {
            // Annotation
            var annotation = new Annotation(text);
            _pipeline.annotate(annotation);
            using (var stream = new ByteArrayOutputStream())
            {
                try
                {
                    _pipeline.jsonPrint(annotation, new PrintWriter(stream));
                    var json = stream.toString();
                    return JsonConvert.DeserializeObject<AnnotationResultDto>(json);
                }
                finally
                {
                    stream.close();
                }
            }
        }

        /// <summary>
        /// Parse sentences from text.
        /// </summary>
        /// <param name="text">Input text.</param>
        /// <returns>Sentences from text.</returns>
        public List<Sentence> GetSentences(string text)
        {
            var document = new Document(text);
            var sentences = document.sentences();

            var result = Enumerable.Range(0, sentences.size())
                .Select(t => (Sentence)sentences.get(t))
                .ToList();

            return result;
        }
    }
}